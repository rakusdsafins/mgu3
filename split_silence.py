from scipy.io import wavfile
from scipy import signal
import numpy as np
import math

sample_rate = 16000

noise_files = [
	'doing_the_dishes.wav',
	'dude_miaowing.wav',
	'exercise_bike.wav',
	'pink_noise.wav',
	'running_tap.wav',
	'white_noise.wav'
]

for path in noise_files:
	base = path.split('.')[0]

	audio_array = wavfile.read(path)[1]

	number_of_files = audio_array.shape[0] // sample_rate
	new_length = number_of_files * sample_rate

	audio_array = audio_array[:new_length]

	arrays = np.array_split(audio_array, number_of_files)
	for index, array in enumerate(arrays):
		wavfile.write(f'silence/{base}_nohash_{index}.wav', 16000, array.astype(np.int16))
