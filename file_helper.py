import pandas as pd


class FileHelper:
    def __init__(self):
        self.label_to_class_dict = {
            'yes': 0,
            'no': 1,
            'up': 2, 
            'down': 3, 
            'left': 4,
            'right': 5,
            'on': 6,
            'off': 7,
            'stop': 8,
            'go': 9,
            'silence': 10 # unkown will be assigned 11
        }
        self.unkown_label = 11

    def get_audio_paths(self, path):
        df = pd.read_csv(path, header=None, names=['path'])
        
        return df

    def assign_labels(self, df):
        df['label'] = df['path'].str.split('/').str[0]
        
        return df

    def convert_labels(self, df):
        df['label'] = (df['label'].map(self.label_to_class_dict)
            .fillna(self.unkown_label).astype(int)
            )

        return df

    def shuffle_df(self, df):
        return df.sample(frac=1).reset_index(drop=True)

    def keep_percentage_of_class(self, df, label=11, percentage = 0.05):
        class_df = df[df['label'] == label]
        rest_df = df[df['label'] != label]

        class_df = class_df.sample(frac=percentage)

        return pd.concat([class_df, rest_df]).sample(frac=1).reset_index(drop=True)

    def create_audio_df(self, path, reduce_label=None, percentage = 0.05):
        df = self.get_audio_paths(path)
        df = self.assign_labels(df)
        df = self.convert_labels(df)
        df = self.shuffle_df(df)
        
        if reduce_label:
            df = self.keep_percentage_of_class(df, reduce_label, percentage)

        return df

    def create_kaggle_csv(self, filenames, classes, path):
        df = pd.DataFrame({
            'fname': filenames,
            'label': classes
        })
        inverted_dict = {value: key for key, value in self.label_to_class_dict.items()}
        df['label'] = df['label'].map(inverted_dict)
        df.to_csv(path, index=False)