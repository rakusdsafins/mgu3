
def merge_files(file1, file2):
	with open(file1, 'a') as f1:
		with open(file2) as f2:
			for cnt, line in enumerate(f2):
				if len(line) > 0:
					f1.write(line)

def merge_validation_train(validation1, validation2, train1, train2):
	merge_files(validation1, validation2)
	merge_files(train1, train2)


val1 = 'validation_list.txt'
val2 = 'silence/validation_silence.txt'

merge_files(val1, val2)
