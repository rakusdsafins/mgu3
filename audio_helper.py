from scipy.io import wavfile
from scipy import signal
import numpy as np
import math
import librosa

class AudioHelper:
    def __init__(self):
        self.sample_count = 16_000 # 16k measurements per sec     

    def _read_audio_file(self, path):
        audio_array = wavfile.read(path)[1]
        if audio_array.shape[0] < self.sample_count:
            sample_diff = self.sample_count - audio_array.shape[0]
            audio_array = np.pad(audio_array,
                (math.floor(sample_diff/2),math.ceil(sample_diff/2)),
                mode='constant')

        return audio_array

    def get_mfcc_spectrograms(self, audio_arrays, n_mfcc=13):
        audio_arrays = audio_arrays.astype(float)
        mfccs = np.apply_along_axis(librosa.feature.mfcc, 1,
            audio_arrays, n_mfcc=13)

        deltas = np.apply_along_axis(librosa.feature.delta, 1, mfccs,
            order=2)

        return deltas

    def inject_noise(self, audio_arrays, factor):
        augmented = []
        for audio in audio_arrays:
            noise = np.random.randn(len(audio))
            augmented_audio = audio + factor * noise
            augmented_audio = augmented_audio.astype(int)
            augmented.append(augmented_audio)

        return np.array(augmented)

    def shift_voice(self, audio_arrays, shift_min, shift_max):
        augmented = []
        for audio in audio_arrays:
            shift = np.random.randint(self.sample_count * shift_min,
                self.sample_count * shift_max)

            augmented_audio = np.roll(audio, shift)
            if np.random.randint(0, 2) == 1:
                augmented_audio[:shift] = 0 # delete beginning
            else:
                augmented_audio[-shift:] = 0 # delete ending

            augmented.append(augmented_audio)

        return np.array(augmented)
            

    def change_pitch(self, audio_arrays, pitch_factor):
        return np.apply_along_axis(librosa.effects.pitch_shift, 1,
            audio_arrays, self.sample_count, pitch_factor)

    def stretch_time(self, audio_arrays, speed_factor):
        return np.apply_along_axis(librosa.effects.time_stretch, 1,
            audio_arrays, speed_factor)

    def get_mel_spectrograms(self, audio_arrays, n_mels=128):
        audio_arrays = audio_arrays.astype(float)
        spectrograms = np.apply_along_axis(librosa.feature.melspectrogram, 1,
            audio_arrays, sr=self.sample_count, n_mels=n_mels)
        
        max_val = np.max(spectrograms)
        
        log_spectrograms = np.apply_along_axis(librosa.power_to_db, 1,
            spectrograms, ref=max_val)

        return log_spectrograms

    def read_audio_files(self, paths):
        return np.array([ self._read_audio_file(path) for path in paths])

    def split_into_timesteps(self, audio_arrays, timesteps):
        return np.apply_along_axis(self._split_one_into_timesteps, 1,   
            audio_arrays, timesteps=timesteps)

    def _split_one_into_timesteps(self, audio_array, timesteps):
        features = int(audio_array.shape[0] / timesteps)
        return audio_array.reshape(timesteps, features)

    def get_spectograms(self, audio_arrays, window_size=20, step_size=10,
        eps=1e-10):
        
        return np.apply_along_axis(self._get_spectogram, 1, audio_arrays,
            window_size=window_size, step_size = step_size, eps = eps)

    def _get_spectogram(self, audio, window_size=20, step_size=10, eps=1e-10):
        nperseg = int(round(window_size * self.sample_count / 1e3))
        noverlap = int(round(step_size * self.sample_count / 1e3))

        # freqs, times, spec = signal.spectogram...
        _, _, spec = signal.spectrogram(audio,
            fs=self.sample_count,
            window='hann',
            nperseg=nperseg,
            noverlap=noverlap,
            detrend=False)
        
        return np.log(spec.T.astype(np.float32) + eps)

    def normalize_per_frequency(self, spectograms_array):
        mean = np.mean(spectograms_array, axis=(0,1))
        std = np.std(spectograms_array, axis=(0,1))
        spectograms_array = (spectograms_array - mean) / std

        return spectograms_array, mean, std

    def normalize(self, spectograms_array):
        mean = np.mean(spectograms_array)
        std = np.std(spectograms_array)
        spectograms_array = (spectograms_array - mean) / std

        return spectograms_array, mean, std

    def normalize_using(self, spectograms_array, mean, std):
        return (spectograms_array - mean) / std