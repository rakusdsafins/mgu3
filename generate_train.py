import os

train_to_save = 'train/train_list.txt'

path = 'train/audio/'
validation = 'train/validation_list.txt'
test = 'train/testing_list.txt'

excluded_files = set()
with open(validation) as file:
	for cnt, line in enumerate(file):
		excluded_files.add(line[:-1])
with open(test) as file:
	for cnt, line in enumerate(file):
		excluded_files.add(line[:-1])

print(len(excluded_files))

train = open(train_to_save, 'w')
for r, d, f in os.walk(path):
	current_dir = r.split('/')[-1]

	if current_dir == '_background_noise_':
		continue

	for file in f:
		file_path = f'{current_dir}/{file}'
		if file_path in excluded_files:
			continue
		train.write(f'{file_path}\n')

train.close()
