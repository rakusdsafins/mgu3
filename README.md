W repozytorium znajduje się kod ułatwiający współpracę z danymi na potrzeby konkursu https://www.kaggle.com/c/tensorflow-speech-recognition-challenge.
### Jupyter Notebooki zawierają kod tworzący sieci neuronowe użyte w konkursie: ###
* https://colab.research.google.com/drive/1E0nG_8Iy9ieXaRAlkZgtxv_jdUIWraE6?usp=sharing
* https://colab.research.google.com/drive/1TRTyQbwutzF0EC0WOH_EV40nXRZfr2bk?usp=sharing
* https://colab.research.google.com/drive/1PDelKmy19eJocns-XXp2AM568rbytPqK?usp=sharing
* https://colab.research.google.com/drive/1-Bg-jK6P9FWyW2GvW9lcohWxTd4Aacz8?usp=sharing
* https://colab.research.google.com/drive/1wPBEUDFl5OYvF3AuWj3_JvokFFNoRKeU?usp=sharing