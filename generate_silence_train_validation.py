from scipy.io import wavfile
import numpy as np
import os


train_validation = 0.8
silence_validation = 'validation_silence.txt'
silence_train = 'train_silence.txt'
sample_rate = 16000

path = 'silence/'

noise_files = {}

for r, d, f in os.walk(path):
	for file in f:
		num = int(file[:-4].split("_")[-1])
		print(num)
		raw_name = '_'.join(file.split("_")[:-2])
		print(raw_name)

		if raw_name in noise_files:
			noise_files[raw_name] = max(noise_files[raw_name], num)
		else:
			noise_files[raw_name] = num


validation = open(silence_validation, "w")
train = open(silence_train, "w")
for name in noise_files.keys():
	print(name)
	print(noise_files[name])
	train_limit = noise_files[name] * train_validation
	print(train_limit)
	for i in range(1, noise_files[name] + 1):
		file_path = f'{path}{name}_nohash_{i}.wav'
		if i < train_limit:
			train.write(f'{file_path}\n')
		else:
			validation.write(f'{file_path}\n')

validation.close()
train.close()
